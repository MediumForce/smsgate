package life.sliwki.smsgate.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Класс ответа на запрос {@link life.sliwki.smsgate.interfaces.LifeSliwkiApi#registerGate(String, PhoneRequest)}
 * Created by Medium on 22.07.2016.
 */
public class RegResponse {

    /**
     * Телефонный номер на который был зарегистрирован смс-шлюз
     */
    @SerializedName("phone_number")
    @Expose
    private String phone;

    /**
     * Код активации который должен придти через смс
     */
    @SerializedName("verification_code")
    @Expose
    private String code;

    /**
     * Если запрос был не успешен то ответ будет содержать этот параметр
     */
    @SerializedName("status")
    @Expose
    private boolean status;

    /**
     * Описание проблемы не успешного запроса
     */
    @SerializedName("description")
    @Expose
    private String description;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "RegResponse{" +
                "phone='" + phone + '\'' +
                ", code='" + code + '\'' +
                ", status=" + status +
                ", description='" + description + '\'' +
                '}';
    }
}

package life.sliwki.smsgate;

import android.Manifest;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import life.sliwki.smsgate.interfaces.LifeSliwkiApi;
import life.sliwki.smsgate.services.PingService;
import life.sliwki.smsgate.services.SmsService;

public class MainActivity extends AppCompatActivity {

    /**
     * Код запроса на получение разрешения
     */
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    private GlobalSetting settings;
    private BroadcastReceiver receiverPing;
    private BroadcastReceiver receiverSms;

    private ProgressBar progressBar;
    private TextView gateStatus;
    private TextView dateReceive;
    private TextView datePing;
    private View activate;
    private View receiver;
    private View available;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        smsReceiveWrapper();

        settings = new GlobalSetting(this);
        final HttpRequest http = new HttpRequest(getApplicationContext());
        Button btnPing = (Button) findViewById(R.id.btn_ping);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        gateStatus = (TextView) findViewById(R.id.text_gate_status);
        dateReceive = (TextView) findViewById(R.id.text_time_sms);
        datePing = (TextView) findViewById(R.id.text_date);
        activate = findViewById(R.id.view_activate);
        receiver = findViewById(R.id.view_receiver);
        available = findViewById(R.id.view_ping);

        final Handler handler = new UpdateUIHandler(this);
        handler.handleMessage(handler.obtainMessage(LifeSliwkiApi.SUCCESSFUL_RESPONSE));

        receiverPing = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int what = intent.getIntExtra("what", LifeSliwkiApi.ERROR_RESPONSE);
                handler.handleMessage(handler.obtainMessage(what));
            }
        };
        receiverSms = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int what = intent.getIntExtra("what", LifeSliwkiApi.ERROR_RESPONSE);
                handler.handleMessage(handler.obtainMessage(what));
            }
        };
        if (btnPing != null) {
            btnPing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressBar.setVisibility(View.VISIBLE);
                    http.pingHost(settings.getSecret(), handler);
                }
            });
        }
        Intent serviceIntent = new Intent(this, PingService.class);
        startService(serviceIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiverPing),
                new IntentFilter(PingService.PING_RESULT));
        LocalBroadcastManager.getInstance(this).registerReceiver((receiverSms),
                new IntentFilter(SmsService.SMS_RESULT));
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiverPing);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiverSms);
        super.onStop();
    }

    /**
     * Функция проверяющая есть ли требуемые разрешения для перехвата смс.
     * Если их нету то они запрашиваются у пользователя
     */
    private void smsReceiveWrapper() {
        List<String> permissionsNeeded = new ArrayList<>();

        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.RECEIVE_SMS))
            permissionsNeeded.add("SMS Receive");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            ActivityCompat.requestPermissions(this, permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.RECEIVE_SMS, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(MainActivity.this, R.string.toast_text_permission_positive, Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(MainActivity.this, R.string.toast_text_permission_negative, Toast.LENGTH_SHORT)
                            .show();
                    NotificationUtils notificationUtils = new NotificationUtils(this);
                    notificationUtils.sendNotifyReceiveSmsPermission();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Функция проверяет полученно ли разрешение
     *
     * @param permissionsList добавляет в лист разрешений, те разрешения к которым не получен доступ
     * @param permission      название разрешения которое хотим добавить
     * @return если такое разрешение есть или успешно добавлено возвращает true иначе false
     */
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission))
                return false;
        }
        return true;
    }

    /**
     * Функция показывающая AlertDialog
     *
     * @param message    сообщение в alertDialog
     * @param okListener событие переданное на кнопку alertDialog'a
     */
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("CANCEL", null)
                .create()
                .show();
    }


    /**
     * Класс отображающий результаты запросов в UI {@link MainActivity}
     */
    private static class UpdateUIHandler extends Handler {
        private WeakReference<MainActivity> mActivity;
        private NotificationUtils notificationUtils;
        private NotificationManager notificationManager;

        public UpdateUIHandler(MainActivity activity) {
            this.mActivity = new WeakReference<>(activity);
            notificationUtils = new NotificationUtils(activity.getApplicationContext());
            notificationManager = (NotificationManager) activity.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity activity = mActivity.get();
            if (activity != null) {
                updateUI(activity);
                if (msg.what < 200 || msg.what > 300) {
                    if (!notificationUtils.isInternetConnection()) {
                        notificationUtils.sendNotifyInternetError();
                    } else if (msg.what >= 500) {
                        notificationUtils.sendNotifyServerError();
                    } else if (msg.what == LifeSliwkiApi.ERROR_FORBIDDEN) {
                        notificationUtils.sendNotifyForbiddenError();
                    }
                } else {
                    if (activity.settings.isActivate()) {
                        notificationManager.cancel(NotificationUtils.NOTIFY_FORBIDDEN_ERROR);
                    }
                    notificationManager.cancel(NotificationUtils.NOTIFY_SERVER_ERROR);
                    notificationManager.cancel(NotificationUtils.NOTIFY_INTERNET_ERROR);
                }
            }
        }

        /**
         * Обновляет интерфейс {@link MainActivity}
         *
         * @param activity параметр для работы с инициализированным обьектам UI {@link MainActivity}
         */
        private void updateUI(MainActivity activity) {
            activity.progressBar.setVisibility(View.INVISIBLE);

            if (activity.settings.isActivate()) {
                activity.gateStatus.setText(R.string.text_is_activate_positive);
            } else
                activity.gateStatus.setText(R.string.text_is_activate_negative);
            setCircleViewFromBool(activity.activate, activity.settings.isActivate(), activity.getApplicationContext());

            activity.dateReceive.setText(activity.settings.getLastTimeSmsReceive());
            setCircleViewFromBool(activity.receiver, activity.settings.isSmsReceiver(), activity.getApplicationContext());

            activity.datePing.setText(activity.settings.getLastTimeServerAvailable());
            setCircleViewFromBool(activity.available, activity.settings.isAvailable(), activity.getApplicationContext());
        }

        /**
         * Функция отрисовки окружностей в правильном цвете.
         * Отрисовывает в двух цветах: красном и зеленом, которые обозначают степень работы функционала программы
         *
         * @param view       {@link View} в котором нужно отрисовать окружность
         * @param isPositive Если эта переменная = true то отрисовываем окружность зеленым цветом, иначе красным.
         * @param context    {@link Context} в котором будет отрисован объект
         */
        private void setCircleViewFromBool(View view, boolean isPositive, Context context) {
            if (isPositive) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    view.setBackground(ContextCompat.getDrawable(context, R.drawable.positive_circle));
                } else
                    view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.positive_circle));
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    view.setBackground(ContextCompat.getDrawable(context, R.drawable.negative_circle));
                } else
                    view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.negative_circle));
            }
        }
    }
}

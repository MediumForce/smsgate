package life.sliwki.smsgate.receivers;

import android.app.Application;
import android.content.Intent;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import life.sliwki.smsgate.BuildConfig;
import life.sliwki.smsgate.services.PingService;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by Medium on 31.07.2016.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class BootLoaderTest {

    @Test
    public void registerServiceOnDeviceBootComplete() throws Exception {
        Intent intent = new Intent(Intent.ACTION_BOOT_COMPLETED);

        ShadowApplication application = ShadowApplication.getInstance();

        assertTrue("Reboot Listener not registered ",
                application.hasReceiverForIntent(intent));
    }

    @Test
    public void registerServiceOnPackageAdd() throws Exception {
        Intent intent = new Intent(Intent.ACTION_PACKAGE_ADDED);

        ShadowApplication application = ShadowApplication.getInstance();

        assertTrue("Package add Listener not registered ",
                application.hasReceiverForIntent(intent));
    }

    @Test
    public void registerServiceOnPackageReplace() throws Exception {
        Intent intent = new Intent(Intent.ACTION_PACKAGE_REPLACED);

        ShadowApplication application = ShadowApplication.getInstance();

        assertTrue("Package replace Listener not registered ",
                application.hasReceiverForIntent(intent));
    }

    @Test
    public void startPingServiceAfterBoot() throws Exception {
        Application application = RuntimeEnvironment.application;
        Intent expectedService = new Intent(application, PingService.class);
        BootLoader bootLoader = new BootLoader();
        bootLoader.onReceive(application, new Intent(Intent.ACTION_BOOT_COMPLETED));

        Intent serviceIntent = shadowOf(application).getNextStartedService();
        assertNotNull("Service started ", serviceIntent);
        assertEquals("Started service class ", serviceIntent.getComponent(),
                expectedService.getComponent());
    }

    @Test
    public void startPingServiceAfterPackageAdd() throws Exception {
        Application application = RuntimeEnvironment.application;
        Intent expectedService = new Intent(application, PingService.class);
        BootLoader bootLoader = new BootLoader();
        bootLoader.onReceive(application, new Intent(Intent.ACTION_PACKAGE_ADDED));

        Intent serviceIntent = shadowOf(application).getNextStartedService();
        assertNotNull("Service started ", serviceIntent);
        assertEquals("Started service class ", serviceIntent.getComponent(),
                expectedService.getComponent());
    }

    @Test
    public void startPingServiceAfterPackageReplace() throws Exception {
        Application application = RuntimeEnvironment.application;
        Intent expectedService = new Intent(application, PingService.class);
        BootLoader bootLoader = new BootLoader();
        bootLoader.onReceive(application, new Intent(Intent.ACTION_PACKAGE_REPLACED));

        Intent serviceIntent = shadowOf(application).getNextStartedService();
        assertNotNull("Service started ", serviceIntent);
        assertEquals("Started service class ", serviceIntent.getComponent(),
                expectedService.getComponent());
    }
}

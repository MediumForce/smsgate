package life.sliwki.smsgate;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import life.sliwki.smsgate.interfaces.LifeSliwkiApi;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Medium on 01.08.2016.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class GlobalSettingTest {

    private SharedPreferences preferences;

    @Before
    public void setUp() {
        preferences = PreferenceManager.getDefaultSharedPreferences(RuntimeEnvironment.application.getApplicationContext());
    }

    @Test
    public void testSetActivate() throws Exception {
        GlobalSetting setting = new GlobalSetting(RuntimeEnvironment.application.getApplicationContext());
        setting.setActivate(true);
        assertEquals(true, preferences.getBoolean("activate", false));
    }

    @Test
    public void testGetActivate() throws Exception {
        GlobalSetting setting = new GlobalSetting(RuntimeEnvironment.application.getApplicationContext());
        preferences.edit().putBoolean("activate", true).apply();
        assertTrue(setting.isActivate());
    }

    @Test
    public void testSetSecret() throws Exception {
        GlobalSetting setting = new GlobalSetting(RuntimeEnvironment.application.getApplicationContext());
        setting.setSecret("aeaf5956-2cec-46a0-ae80-8fb764c65406");
        assertEquals("aeaf5956-2cec-46a0-ae80-8fb764c65406", preferences.getString("secret", ""));
    }

    @Test
    public void testGetSecret() throws Exception {
        GlobalSetting setting = new GlobalSetting(RuntimeEnvironment.application.getApplicationContext());
        assertEquals(setting.getSecret(), LifeSliwkiApi.DEFAULT_SECRET_KEY);
        preferences.edit().putString("secret", "aeaf5956-2cec-46a0-ae80-8fb764c65406").apply();
        assertEquals(setting.getSecret(), "aeaf5956-2cec-46a0-ae80-8fb764c65406");
    }

    @Test
    public void testSetSmsReceiver() throws Exception {
        GlobalSetting setting = new GlobalSetting(RuntimeEnvironment.application.getApplicationContext());
        setting.setSmsReceiver(true);
        assertEquals(true, preferences.getBoolean("sms_receiver", false));
    }

    @Test
    public void testGetSmsReceiver() throws Exception {
        GlobalSetting setting = new GlobalSetting(RuntimeEnvironment.application.getApplicationContext());
        preferences.edit().putBoolean("sms_receiver", true).apply();
        assertTrue(setting.isSmsReceiver());
    }

    @Test
    public void testSetAvailable() throws Exception {
        GlobalSetting setting = new GlobalSetting(RuntimeEnvironment.application.getApplicationContext());
        setting.setAvailable(true);
        assertEquals(true, preferences.getBoolean("available", false));
    }

    @Test
    public void testGetAvailable() throws Exception {
        GlobalSetting setting = new GlobalSetting(RuntimeEnvironment.application.getApplicationContext());
        preferences.edit().putBoolean("available", true).apply();
        assertTrue(setting.isAvailable());
    }

    @Test
    public void testSetLastTimeServerAvailable() throws Exception {
        GlobalSetting setting = new GlobalSetting(RuntimeEnvironment.application.getApplicationContext());
        setting.setLastTimeServerAvailable("02-04-2011 12:20:40");
        assertEquals("02-04-2011 12:20:40", preferences.getString("time_ping", ""));
    }

    @Test
    public void testGetLastTimeServerAvailable() throws Exception {
        GlobalSetting setting = new GlobalSetting(RuntimeEnvironment.application.getApplicationContext());
        preferences.edit().putString("time_ping", "02-04-2011 12:20:41").apply();
        assertEquals(setting.getLastTimeServerAvailable(), "02-04-2011 12:20:41");
    }

    @Test
    public void testSetLastTimeSmsReceive() throws Exception {
        GlobalSetting setting = new GlobalSetting(RuntimeEnvironment.application.getApplicationContext());
        setting.setLastTimeSmsReceive("02-04-2011 12:20:33");
        assertEquals("02-04-2011 12:20:33", preferences.getString("time_receive", ""));
    }

    @Test
    public void testGetLastTimeSmsReceive() throws Exception {
        GlobalSetting setting = new GlobalSetting(RuntimeEnvironment.application.getApplicationContext());
        preferences.edit().putString("time_receive", "02-04-2011 12:20:44").apply();
        assertEquals(setting.getLastTimeSmsReceive(), "02-04-2011 12:20:44");
    }

    @After
    public void tearDown() {
        preferences.edit().clear().apply();
    }
}

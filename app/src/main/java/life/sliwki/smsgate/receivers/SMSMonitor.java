package life.sliwki.smsgate.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import life.sliwki.smsgate.GlobalSetting;
import life.sliwki.smsgate.HttpRequest;
import life.sliwki.smsgate.interfaces.LifeSliwkiApi;
import life.sliwki.smsgate.services.SmsService;

/**
 *  Класс {@link BroadcastReceiver} перехватывающий смс.
 */
public class SMSMonitor extends BroadcastReceiver {

    /**
     * Пяти секундная задержка перед удалением смс
     */
    private static final long FIVE_SEC = 5 * 1000;

    /**
     * Функция перехвата смс.<br>Если перехваченное смс удовлетворяю условиям
     * {@link #isReceiverCode(String smsCodeString)} или {@link #isActivateCode(String smsCodeString)} то смс будет
     * передано в {@link SmsService} через {@link Intent} с кодами запросов которые должны быть выполнены
     * в этом сервисе.
     * @param context {@link Context} с которым receiver запускается
     * @param intent {@link Intent} который был перехвачен receiverом
     */
    @Override
    public void onReceive(final Context context, Intent intent) {

        GlobalSetting settings = new GlobalSetting(context);

        if (intent != null && intent.getAction() != null &&
                Telephony.Sms.Intents.SMS_RECEIVED_ACTION.compareToIgnoreCase(intent.getAction()) == 0) {

            SmsMessage[] messages;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                messages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            } else {
                Object[] pduArray = (Object[]) intent.getExtras().get("pdus");
                messages = new SmsMessage[pduArray.length];
                for (int i = 0; i < pduArray.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pduArray[i]);
                }
            }

            final StringBuilder bodyText = new StringBuilder();
            for (SmsMessage message : messages) {
                bodyText.append(message.getMessageBody());
            }

            final String number = messages[0].getDisplayOriginatingAddress();

            final String text = bodyText.toString();
            if (isActivateCode(text) || isReceiverCode(text)) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT)
                    deleteSms(context, number, text);
                settings.setLastTimeSmsReceive(HttpRequest.getCurrentTimeString());
                Intent intentService = new Intent(context, SmsService.class);
                intentService.putExtra("text", text);
                intentService.putExtra("phone", number);
                if (isActivateCode(text))
                    intentService.putExtra("request", LifeSliwkiApi.ACTIVATE_REQUEST_CODE);
                if (isReceiverCode(text))
                    intentService.putExtra("request", LifeSliwkiApi.RECEIVER_REQUEST_CODE);
                context.startService(intentService);
            } else abortBroadcast();
        }
    }

    /**
     * Функция удаляет смс в новом потоке при совпадении номера отправителя и текста сообщения.
     * <br>Чтобы удалить смс, происходит задержка в 5 секунд т.к. смс попадает в БД не сразу.
     * <br>Работает на версиях android ниже {@link Build.VERSION_CODES#KITKAT}
     *
     * @param context {@link Context} с которым работает {@link android.content.ContentResolver}
     * @param number Телефонный номер сообщения которое должно быть удалено
     * @param text Текст смс сообщения которое должно быть удалено
     */
    public void deleteSms(final Context context, final String number, final String text){
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(FIVE_SEC);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Uri uriSms = Uri.parse("content://sms/inbox");
                Cursor c = context.getContentResolver().query(uriSms,
                        new String[] { "_id", "thread_id", "address",
                                "person", "date", "body" }, null, null, null);
                if (c != null && c.moveToFirst()) {
                    do {
                        long id = c.getLong(0);
                        String address = c.getString(2);
                        String body = c.getString(5);
                        if (text.equals(body) && address.equals(number)) {
                            Log.v("Deleting SMS with id", "" + id);
                            context.getContentResolver().delete(
                                    Uri.parse("content://sms/" + id), null, null);
                        }
                    } while (c.moveToNext());
                    c.close();
                }
            }
        };
        new Thread(run).start();
    }

    /**
     * Функция проверяющая содержит ли текст смс только цифры
     * @param smsCodeString содержит текст смс
     * @return true если текст смс содержит только цифры, иначе false
     */
    public static boolean isReceiverCode(String smsCodeString) {
        Pattern p = Pattern.compile("^[0-9]+$");
        Matcher m = p.matcher(smsCodeString);
        return m.matches();
    }

    /**
     * Функция проверяющая является ли текст смски регистрационной
     * @param smsCodeString содержит текст смс
     * @return true если текст смс содержит вначале "reg-" и только цифры после тире, иначе false
     */
    public static boolean isActivateCode(String smsCodeString) {
        Pattern p = Pattern.compile("^reg-[0-9]+$");
        Matcher m = p.matcher(smsCodeString);
        return m.matches();
    }

}

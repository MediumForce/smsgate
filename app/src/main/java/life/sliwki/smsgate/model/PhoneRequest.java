package life.sliwki.smsgate.model;

import com.google.gson.annotations.SerializedName;

/**
 * Класс тела запросов {@link life.sliwki.smsgate.interfaces.LifeSliwkiApi#registerGate(String, PhoneRequest)}
 * и {@link life.sliwki.smsgate.interfaces.LifeSliwkiApi#deleteGate(String, PhoneRequest)}
 * Created by Medium on 21.07.2016.
 */
public class PhoneRequest {

    /**
     * Телефонный номер смс-шлюза который будет зарегистрирован или удален
     */
    @SerializedName("phone_num")
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

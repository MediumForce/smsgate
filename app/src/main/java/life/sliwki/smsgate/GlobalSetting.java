package life.sliwki.smsgate;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import life.sliwki.smsgate.interfaces.LifeSliwkiApi;

/**
 * Класс содержащий работу с глобальными настройками приложения.<br>
 * Created by serjm on 21.04.2016.<br>
 * Edited by medium on 29.07.2016.
 */
public class GlobalSetting {
    /**
     * {@link SharedPreferences} хранения настроек
     */
    private SharedPreferences settings;

    /**
     * {@link Context} в котором будут доступны настройки
     */
    private Context context;

    /**
     * Конструктор создающий {@link SharedPreferences} из {@link Context}
     *
     * @param context в котором будет создан экземпляр{@link GlobalSetting}
     */
    public GlobalSetting(Context context) {
        this.settings = PreferenceManager.getDefaultSharedPreferences(context);
        this.context = context;
    }

    /**
     * Сохраняет в настройках является ли смс-шлюз зарегестрированным.
     */
    public void setRegister(Boolean bool) {
        editSettings("register", bool);
    }

    /**
     * @return Если sms-шлюз не зарегистрирован или приложение было только установлено то функция
     * вернет false иначе true.
     */
    public Boolean isRegister() {
        return settings.getBoolean("register", false);
    }

    /**
     * Сохраняет в настройках является ли смс-шлюз активированным.
     */
    public void setActivate(Boolean bool) {
        editSettings("activate", bool);
    }

    /**
     * @return Если sms-шлюз не активирован или приложение было только установлено то функция
     * вернет false иначе true.
     */
    public Boolean isActivate() {
        return settings.getBoolean("activate", false);
    }

    /**
     * @return Секретный код для запросов, если смс-шлюз не был активирован то возвращает
     * {@link LifeSliwkiApi#DEFAULT_SECRET_KEY}
     */
    public String getSecret() {
        return settings.getString("secret", LifeSliwkiApi.DEFAULT_SECRET_KEY);
    }

    /**
     * Сохраняет в настройках секретный код смс-шлюза
     *
     * @param code текст секретного кода
     */
    public void setSecret(String code) {
        editSettings("secret", code);
    }

    /**
     * Сохраняет в настройках получил ли сервер последнию перехваченную смс.
     */
    public void setSmsReceiver(Boolean bool) {
        editSettings("sms_receiver", bool);
    }

    /**
     * @return false если приложение еще не отсылало никаких смс или последняя смс не была принята сервером
     */
    public Boolean isSmsReceiver() {
        return settings.getBoolean("sms_receiver", false);
    }

    /**
     * Сохраняет в настройках доступен ли был сервер при последней попытке {@link LifeSliwkiApi#pingHost(String)}
     */
    public void setAvailable(Boolean bool) {
        editSettings("available", bool);
    }

    /**
     * @return true если последнее соединение с сервером было успешно, иначе false
     */
    public Boolean isAvailable() {
        return settings.getBoolean("available", false);
    }

    /**
     * Сохраняет в настройках время последнего успешного соединения с сервером
     *
     * @param time время соединение в виде {@link String}
     */
    public void setLastTimeServerAvailable(String time) {
        editSettings("time_ping", time);
    }

    /**
     * @return {@link String} текст времени последнего успешного соединения с сервером
     */
    public String getLastTimeServerAvailable() {
        return settings.getString("time_ping", context.getString(R.string.text_time_ping_default));
    }

    /**
     * Сохраняет в настройках время последней перехваченной смс
     *
     * @param time время в виде {@link String}
     */
    public void setLastTimeSmsReceive(String time) {
        editSettings("time_receive", time);
    }

    /**
     * @return {@link String} текст времени последней перехваченной смс
     */
    public String getLastTimeSmsReceive() {
        return settings.getString("time_receive", context.getString(R.string.text_time_receive_default));
    }

    /**
     * Функция очищения настроек приложения
     */
    public void clear() {
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.apply();
    }

    /**
     * Функция удаления определенного параметра настроек
     *
     * @param key название параметра который подлежит удалению
     */
    public void remove(String key) {
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.apply();
    }

    /**
     * Сохраняет строковый параметр настроек с заданным названием
     *
     * @param name  название параметра настройки
     * @param param значение параметра
     */
    private void editSettings(String name, String param) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, param);
        editor.apply();
    }

    /**
     * Сохраняет long параметр настроек с заданным названием
     *
     * @param name  название параметра настройки
     * @param param значение параметра
     */
    private void editSettings(String name, long param) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(name, param);
        editor.apply();
    }

    /**
     * Сохраняет float параметр настроек с заданным названием
     *
     * @param name  название параметра настройки
     * @param param значение параметра
     */
    private void editSettings(String name, float param) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(name, param);
        editor.apply();
    }

    /**
     * Сохраняет int параметр настроек с заданным названием
     *
     * @param name  название параметра настройки
     * @param param значение параметра
     */
    private void editSettings(String name, int param) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(name, param);
        editor.apply();
    }

    /**
     * Сохраняет boolean параметр настроек с заданным названием
     *
     * @param name  название параметра настройки
     * @param param значение параметра
     */
    private void editSettings(String name, Boolean param) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(name, param);
        editor.apply();
    }
}

package life.sliwki.smsgate.interfaces;

import life.sliwki.smsgate.model.ActivateRequest;
import life.sliwki.smsgate.model.ActivateResponse;
import life.sliwki.smsgate.model.DeleteResponse;
import life.sliwki.smsgate.model.PhoneRequest;
import life.sliwki.smsgate.model.PingResponse;
import life.sliwki.smsgate.model.RegResponse;
import life.sliwki.smsgate.model.SmsRequest;
import life.sliwki.smsgate.model.SmsResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Этот интерфейс описывает запросы к сервису sliwki.life с помощью библиотеки Retrofit 2.0
 * Created by Medium on 21.07.2016.
 */
public interface LifeSliwkiApi {
    /**
     * Базовый адрес на котором располагаются адреса запросов
     */
    String BASE_URL = "https://sliwki.life/api/sms_gateway/";
    /**
     * Секретный ключ по умолчанию, используется в запросах к серверу когда смс-шлюз не прошел активацию
     */
    String DEFAULT_SECRET_KEY = "unregistered_gateway";

    /**
     * Код запроса {@link #activateGate(String, ActivateRequest)} для передачи в {@link android.content.Intent}
     */
    int ACTIVATE_REQUEST_CODE = 1;
    /**
     * Код запроса {@link #receiver(String, SmsRequest)} для передачи в {@link android.content.Intent}
     */
    int RECEIVER_REQUEST_CODE = 2;
    /**
     * Код запроса {@link #pingHost(String)} для передачи в {@link android.content.Intent}
     */
    int PING_REQUEST_CODE = 3;

    /**
     * Ошибка доступа, происходит если был передан неправильный секретный ключ
     */
    int ERROR_FORBIDDEN = 403;

    /**
     * Непредвиденная ошибка, код такой ошибки передается в {@link retrofit2.Callback#onFailure(Call, Throwable)}
     */
    int ERROR_RESPONSE = 434;

    /**
     * HTTP код успешного ответа
     */
    int SUCCESSFUL_RESPONSE = 200;

    /**
     * Запрос на регистрацию Смс-шлюза.
     * @param secret секретный ключ, по умолчанию должен принимать значение: {@link #DEFAULT_SECRET_KEY}
     * @param phone телефонный номер смс-шлюза в виде объекта {@link PhoneRequest}, который будет зарегистриован
     * @return Ответ в виде объекта {@link RegResponse} в котором при успешном ответе будет храниться
     * отправленный телефон.<br>В случае неудачи {@link RegResponse#status} = false.
     * Если телефон уже зарегистрирован то в {@link RegResponse#description} будет сказано об этом.
     */
    @Headers("Content-Type: application/json")
    @POST("register/")
    Call<RegResponse> registerGate(@Header("Secret-Code") String secret, @Body PhoneRequest phone);

    /**
     * Запрос на Активацию Смс-шлюза.
     * @param secret секретный ключ по умолчанию должен принимать значение: {@link #DEFAULT_SECRET_KEY}
     * @param activateRequest содержит код смс активации {@link ActivateRequest#code} и телефон
     * откуда оно пришло {@link ActivateRequest#phone}
     * @return Ответ в виде объекта {@link ActivateResponse} в котором при успешном ответе будет
     * секретный код {@link ActivateResponse#secretCode} который будет применяться для всех запросов
     * вместо ключа по умолчанию {@link #DEFAULT_SECRET_KEY}.<br>При успешном ответе
     * {@link ActivateResponse#approved} = true.<br>В случае неудачи {@link ActivateResponse#status} = false.
     */
    @Headers("Content-Type: application/json")
    @POST("activate/")
    Call<ActivateResponse> activateGate(@Header("Secret-Code") String secret, @Body ActivateRequest activateRequest);

    /**
     * Временный запрос для теста, удаляет Смс-шлюз из сервера.
     * @param secret секретный ключ по умолчанию должен принимать значение: {@link #DEFAULT_SECRET_KEY}
     * @param phone телефонный номер смс-шлюза в виде объекта {@link PhoneRequest} который будет удален
     * @return Ответ в виде объекта {@link DeleteResponse}, где при {@link DeleteResponse#status} =
     * true удаление произошло успешно.
     */
    @Headers("Content-Type: application/json")
    @POST("delete/")
    Call<DeleteResponse> deleteGate(@Header("Secret-Code") String secret, @Body PhoneRequest phone);

    /**
     * Запрос для отправки полученных смс.<br>Для отправки этого запроса смс-шлюз должен быть активирован.
     * @param secret секретный ключ который должен был быть получен с помощью запроса {@link #activateGate(String, ActivateRequest)}
     * @param sms содержит текст смс {@link SmsRequest#text} и телефон откуда оно пришло {@link SmsRequest#phone}
     * @return Ответ в виде объекта {@link SmsResponse}, где при {@link SmsResponse#status} =
     * true смс было доставлено успешно.
     */
    @Headers("Content-Type: application/json")
    @POST("receiver/")
    Call<SmsResponse> receiver(@Header("Secret-Code") String secret, @Body SmsRequest sms);

    /**
     * Запрос для проверки связи с сервером.<br>Для отправки этого запроса смс-шлюз должен быть активирован.
     * @param secret секретный ключ который должен был быть получен с помощью запроса {@link #activateGate(String, ActivateRequest)}
     * @return Ответ в виде объекта {@link PingResponse}, где при {@link PingResponse#status} =
     * true сервер доступен.
     */
    @Headers("Content-Type: application/json")
    @POST("ping/")
    Call<PingResponse> pingHost(@Header("Secret-Code") String secret);

}

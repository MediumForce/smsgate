package life.sliwki.smsgate.services;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;

import life.sliwki.smsgate.GlobalSetting;
import life.sliwki.smsgate.HttpRequest;
import life.sliwki.smsgate.NotificationUtils;
import life.sliwki.smsgate.interfaces.LifeSliwkiApi;

/**
 * Класс {@link Service} отправляющий запросы содержащие смс на сервер
 */
public class SmsService extends Service {

    /**
     * Параметр для {@link Intent} сообщающий что UI в {@link life.sliwki.smsgate.MainActivity} что
     * UI надо обновиться в соответствием с результатами запросов
     */
    public final static String SMS_RESULT = "life.sliwki.smsgate.services.SmsService.REQUEST_PROCESSED";

    /**
     * Переменная настроек для получения секретного кода
     */
    private GlobalSetting settings;

    /**
     * {@link NotificationManager} для работы с уведомлениями
     */
    private NotificationManager mNotificationManager;
    /**
     * {@link NotificationUtils} для вызова уведомлений
     */
    private NotificationUtils notificationUtils;

    /**
     * Класс помощник позволяющий делать запросы к серверу
     */
    private HttpRequest http;

    @Override
    public void onCreate() {
        super.onCreate();
        settings = new GlobalSetting(getApplicationContext());
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        http = new HttpRequest(getApplicationContext());
        this.notificationUtils = new NotificationUtils(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Функция обрабатывает переданный {@link Intent} из {@link life.sliwki.smsgate.receivers.SMSMonitor}
     * и отсылает на сервер смс указанным в {@link Intent} запросом.
     * @param intent который был передан при вызове сервиса
     * @param flags дополнительной информации о запущенном запросе
     * @param startId номер выполняемого запроса сервиса
     * @return Возвращаемое значение указывает на то должен ли сервис использовать текущее начальное состояние
     */
    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        if (intent != null) {
            String number = intent.getExtras().getString("phone");
            String text = intent.getExtras().getString("text");
            int request = intent.getIntExtra("request", 0);
            Handler handler = new RestartServiceHandler(this, intent, mNotificationManager, notificationUtils, settings);
            if (request == LifeSliwkiApi.ACTIVATE_REQUEST_CODE)
                http.activateRequest(number, text, handler);

            if (request == LifeSliwkiApi.RECEIVER_REQUEST_CODE)
                http.receiverRequest(settings.getSecret(), number, text, handler);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * Обработчик результата запроса, призванный повторно производить запрос если предыдущий окончился неудачно
     */
    private static class RestartServiceHandler extends Handler {
        private NotificationUtils notificationUtils;
        private Intent intent;
        private Context context;
        private LocalBroadcastManager broadcaster;
        private NotificationManager mNotificationManager;
        private GlobalSetting setting;
        private static final long ONE_MIN = 60 * 1000;

        /**
         * Создает экземпляр {@link Handler} для обработки результатов смс запросов.
         * @param context в котором создается {@link Handler}
         * @param intent который был выполнен {@link SmsService}
         * @param notificationManager для отмены уведомлений в случае устранения ошибочных ситуаций
         * @param notificationUtils для вызова уведомлений
         * @param settings для доступа к настройкам приложения
         */
        private RestartServiceHandler(Context context, Intent intent, NotificationManager notificationManager,
                                      NotificationUtils notificationUtils, GlobalSetting settings) {
            this.context = context;
            this.intent = intent;
            this.mNotificationManager = notificationManager;
            this.broadcaster = LocalBroadcastManager.getInstance(context);
            this.notificationUtils = notificationUtils;
            this.setting = settings;
        }

        /**
         * Функция выдающая уведомление соответствующие коду состояния http запроса в случае если {@link Message#what}
         * не является успешным кода запроса, а также отправляет повторный запрос заново через одну минуту. <br>
         * Если запрос был отправлен успешно то отправляет на UI {@link life.sliwki.smsgate.MainActivity} {@link Intent}
         * об этом и отменяет уведомления не соответвующие текущему состоянию приложения
         * @param msg {@link Message} содержащий код состояния http запроса
         */
        public void handleMessage(Message msg) {
            if (msg.what < 200 || msg.what > 300) {
                if (!notificationUtils.isInternetConnection()) {
                    notificationUtils.sendNotifyInternetError();
                } else if (msg.what >= 500) {
                    notificationUtils.sendNotifyServerError();
                } else if (msg.what == LifeSliwkiApi.ERROR_FORBIDDEN) {
                    notificationUtils.sendNotifyForbiddenError();
                }

                Runnable run = new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(ONE_MIN);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        context.startService(intent);
                    }
                };
                new Thread(run).start();

            } else {
                if (setting.isActivate()) {
                    mNotificationManager.cancel(NotificationUtils.NOTIFY_FORBIDDEN_ERROR);
                }
                mNotificationManager.cancel(NotificationUtils.NOTIFY_SERVER_ERROR);
                mNotificationManager.cancel(NotificationUtils.NOTIFY_INTERNET_ERROR);
            }
            Intent intent = new Intent(SMS_RESULT);
            intent.putExtra("what", msg.what);
            broadcaster.sendBroadcast(intent);
        }
    }
}

package life.sliwki.smsgate;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

/**
 * Класс помощник вызывающий уведомления
 * Created by Medium on 27.07.2016.
 */
public class NotificationUtils {

    /**
     * Код уведомления о нехватающем разрешении приложению на перехват смс в системе
     */
    public static final int NOTIFY_RECEIVE_SMS_PERMISSIONS = 123;

    /**
     * Код уведомления при отсутствии соединения к интернету
     */
    public static final int NOTIFY_INTERNET_ERROR = 124;

    /**
     * Код уведомления когда происходит ошибка на сервере
     */
    public static final int NOTIFY_SERVER_ERROR = 125;

    /**
     * Код уведомления об отуствии прав
     */
    public static final int NOTIFY_FORBIDDEN_ERROR = 126;

    /**
     * Код foreground уведомления
     */
    public static final int NOTIFY_FOREGROUND = 127;

    /**
     * Текстовый тэг для записи логов
     */
    private static final String TAG = "SmsGateService";

    /**
     * {@link Context} в котором работает {@link NotificationUtils}
     */
    private Context context;

    /**
     * {@link NotificationManager} для работы с уведомлениями
     */
    private NotificationManager notificationManager;

    /**
     * Создает экземпляр {@link NotificationUtils} для запуска уведомлений.
     *
     * @param context в котором создается {@link NotificationUtils}
     */
    public NotificationUtils(Context context) {
        this.context = context;
        this.notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    /**
     * Отправляет уведомления об отсутвии разрешения на доступ к смс
     */
    public void sendNotifyReceiveSmsPermission() {
        NotificationCompat.Builder builder =
                buildNotifyWithHandsUp(context.getString(R.string.notify_title_receive_sms_permission),
                        context.getString(R.string.notify_description_receive_sms_permission));
        Intent resultIntent = new Intent(context, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);
        notificationManager.notify(NOTIFY_RECEIVE_SMS_PERMISSIONS, builder.build());
        Log.v(TAG, "Permission receive sms was denied");
    }

    /**
     * Отправляет уведомление об отсутствии соединения с интернетом
     */
    public void sendNotifyInternetError() {
        NotificationCompat.Builder builder =
                buildNotifyWithoutIntent(context.getString(R.string.notify_title_internet_error), context.getString(R.string.notify_description_internet_error));
        notificationManager.notify(NOTIFY_INTERNET_ERROR, builder.build());
        Log.v(TAG, "Internet connection is not available");
    }

    /**
     * Отправляет уведомление при ошибки на сервере
     */
    public void sendNotifyServerError() {
        NotificationCompat.Builder builder =
                buildNotifyWithoutIntent(context.getString(R.string.notify_title_server_error), context.getString(R.string.notify_description_server_error));
        notificationManager.notify(NOTIFY_SERVER_ERROR, builder.build());

        Log.v(TAG, "Server is not available");
    }

    /**
     * Отправляет уведомление об отсутствии прав на отправку запросов на сервер
     */
    public void sendNotifyForbiddenError() {
        NotificationCompat.Builder builder =
                buildNotifyWithoutIntent(context.getString(R.string.notify_title_forbidden_error), context.getString(R.string.notify_description_forbidden_error));
        notificationManager.notify(NOTIFY_FORBIDDEN_ERROR, builder.build());
        Log.v(TAG, "SmsGate is not activate");
    }

    /**
     * Проверяет есть ли соединеине с интернетом
     *
     * @return true если есть соедиениен с интернетом, иначе false
     */
    public boolean isInternetConnection() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /**
     * Создает {@link NotificationCompat.Builder} c произвольным заголовком и описанием с Hands-Up
     *
     * @param title       текст заголовка уведомления
     * @param description описание уведомления
     * @return {@link NotificationCompat.Builder} типа Hands-Up для версий {@link Build.VERSION_CODES#LOLLIPOP}
     */
    public NotificationCompat.Builder buildNotifyWithHandsUp(String title, String description) {
        NotificationCompat.Builder builder = buildSimpleNotify(title, description);
        builder.setTicker(title)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(description));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setPriority(Notification.PRIORITY_MAX)
                    .setDefaults(Notification.DEFAULT_ALL);
        }
        return builder;
    }

    /**
     * Создает простой {@link NotificationCompat.Builder} c произвольным заголовком и описанием
     *
     * @param title       текст заголовка уведомления
     * @param description описание уведомления
     * @return {@link NotificationCompat.Builder} типа Hands-Up для версий {@link Build.VERSION_CODES#LOLLIPOP}
     */
    public NotificationCompat.Builder buildSimpleNotify(String title, String description) {
        return new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(description)
                .setWhen(System.currentTimeMillis());
    }

    /**
     * Создает foreground уведомление которое направляет в {@link MainActivity}
     *
     * @return {@link Notification} foreground уведомления
     */
    public Notification buildNotifyForeground() {
        NotificationCompat.Builder builder = buildSimpleNotify(context.getString(R.string.notify_title_foreground), context.getString(R.string.notify_description_foreground));
        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
        builder.setContentIntent(pendingIntent);
        return builder.build();
    }

    /**
     * Создает {@link NotificationCompat.Builder} c произвольным заголовком и описанием без перехода на {@link android.app.Activity}
     *
     * @param title       текст заголовка уведомления
     * @param description описание уведомления
     * @return {@link NotificationCompat.Builder} типа Hands-Up для версий {@link Build.VERSION_CODES#LOLLIPOP}
     * и без перехода на {@link android.app.Activity}
     */
    public NotificationCompat.Builder buildNotifyWithoutIntent(String title, String description) {
        NotificationCompat.Builder builder = buildNotifyWithHandsUp(title, description);
        PendingIntent contentIntent =
                PendingIntent.getActivity(context,
                        0, new Intent(),
                        PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        return builder;
    }
}

package life.sliwki.smsgate;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Locale;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import life.sliwki.smsgate.interfaces.LifeSliwkiApi;
import life.sliwki.smsgate.model.ActivateRequest;
import life.sliwki.smsgate.model.ActivateResponse;
import life.sliwki.smsgate.model.DeleteResponse;
import life.sliwki.smsgate.model.PhoneRequest;
import life.sliwki.smsgate.model.PingResponse;
import life.sliwki.smsgate.model.RegResponse;
import life.sliwki.smsgate.model.SmsRequest;
import life.sliwki.smsgate.model.SmsResponse;
import okhttp3.OkHttpClient;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Класс retrofit запросов производящий асинхронные запросы к {@link LifeSliwkiApi}
 * Created by Medium on 20.07.2016.
 */
public class HttpRequest {

    /**
     * Интерфейс для вызова запросов к API
     */
    private LifeSliwkiApi api;

    /**
     * {@link Context} в котором будут вызываться запросы
     */
    private Context context;

    /**
     * Переменная настроек для получения секретного кода и сохранения результатов запросов
     */
    private GlobalSetting settings;

    /**
     * Инициализирует Retrofit и создает экземпляр {@link HttpRequest} для запуска запросов
     *
     * @param context {@link Context} в котором будут вызываться запросы
     */
    public HttpRequest(Context context) {
        Gson gson = new GsonBuilder().create();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(LifeSliwkiApi.BASE_URL)
//                .client(getSafeOkHttpClient())
                .build();
        this.api = retrofit.create(LifeSliwkiApi.class);
        this.context = context;
        this.settings = new GlobalSetting(context);
    }


    private OkHttpClient getSafeOkHttpClient() {

        try {
            X509TrustManager trustManager;
            SSLSocketFactory sslSocketFactory;
            try {
                trustManager = trustManagerForCertificates(trustedCertificatesInputStream());
                SSLContext sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, new TrustManager[] { trustManager }, null);
                sslSocketFactory = sslContext.getSocketFactory();
            } catch (GeneralSecurityException e) {
                throw new RuntimeException(e);
            }

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient = okHttpClient.newBuilder().
                    sslSocketFactory(sslSocketFactory, trustManager)
                    .build();

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private X509TrustManager trustManagerForCertificates(InputStream in)
            throws GeneralSecurityException {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        Collection<? extends Certificate> certificates = certificateFactory.generateCertificates(in);
        if (certificates.isEmpty()) {
            throw new IllegalArgumentException("expected non-empty set of trusted certificates");
        }

        char[] password = "password".toCharArray(); // Any password will work.
        KeyStore keyStore = newEmptyKeyStore(password);
        int index = 0;
        for (Certificate certificate : certificates) {
            String certificateAlias = Integer.toString(index++);
            keyStore.setCertificateEntry(certificateAlias, certificate);
        }

        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(
                KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, password);
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);
        TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
        if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
            throw new IllegalStateException("Unexpected default trust managers:"
                    + Arrays.toString(trustManagers));
        }
        return (X509TrustManager) trustManagers[0];
    }

    private KeyStore newEmptyKeyStore(char[] password) throws GeneralSecurityException {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream in = null;
            keyStore.load(in, password);
            return keyStore;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    private InputStream trustedCertificatesInputStream() {
        //Сертификат сервера которому можно доверять
        String serverCertificate= ""
                + "-----BEGIN CERTIFICATE-----\n"
                + "MIIGBjCCBO6gAwIBAgISESEPPQkzFAHdbubtkIaLpQVjMA0GCSqGSIb3DQEBCwUA\n"
                + "MGAxCzAJBgNVBAYTAkJFMRkwFwYDVQQKExBHbG9iYWxTaWduIG52LXNhMTYwNAYD\n"
                + "VQQDEy1HbG9iYWxTaWduIERvbWFpbiBWYWxpZGF0aW9uIENBIC0gU0hBMjU2IC0g\n"
                + "RzIwHhcNMTYwNDA1MjA0NzI2WhcNMTcwNDA2MjA0NzI2WjA9MSEwHwYDVQQLExhE\n"
                + "b21haW4gQ29udHJvbCBWYWxpZGF0ZWQxGDAWBgNVBAMTD3d3dy5zbGl3a2kubGlm\n"
                + "ZTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBANaj386Juo/WK5m6Yn82\n"
                + "/bP6WsVYG8cFW0YGHBSiP09SHFaDmmV4YtKtk04lgCVrCZqqT4aZk1nKNCbw7Ir8\n"
                + "/EOmgstJPqCxLQ8XU858H8aHCuz0k+cxbGQbHdGftPIRbBknRuSnQeVfnmKnYzrK\n"
                + "gje+ER0+tu6d9RnzKR5zM0aLZgRMsA/S7BN5kfbB3Qjiny86RIqprHivviDaQvdb\n"
                + "5Jr3gSCKMZG2n/005JhFWP1NJK2m7ukqAAB6bR6YYzAg18rCeEARdC37mwJXj1yw\n"
                + "j8Iljm0hgso089683dZYwy6okjvUzoIRcEl0K8e25zSg8/JPTWwJgvhz6iugK7oy\n"
                + "d5V5xdWobymO781XWnEvx+cLL0dfJ6I0kRrgrIimBEEFZBGvZPVQYDYDQmdtJrlE\n"
                + "rOHHZnvkX1OsxEZ3saG2fZFVlmZT2mLpR0SevBf1CL+zpNWVWrqbAQVkB9qHRnO5\n"
                + "0sc1/1xDog/Cj3NllvoaSAgm7YV5ag7W+WjreW6qCbcvUASJXzuhYKfqfarDFzKI\n"
                + "irBw3W+BsV5tKb5NnjYczdPTB0sWry4hhLQGXp3m/VzyOOGqPt4p71iS6ER9ye1G\n"
                + "9j6pmn7LG2fbbyMZwMedPtNULSNY/iirPZ4VxwAlAZ9qRj5wvzXL839rSwJNI7tX\n"
                + "90BPk87snSHykEKyILku/zvBAgMBAAGjggHbMIIB1zAOBgNVHQ8BAf8EBAMCBaAw\n"
                + "VgYDVR0gBE8wTTBBBgkrBgEEAaAyAQowNDAyBggrBgEFBQcCARYmaHR0cHM6Ly93\n"
                + "d3cuZ2xvYmFsc2lnbi5jb20vcmVwb3NpdG9yeS8wCAYGZ4EMAQIBMCcGA1UdEQQg\n"
                + "MB6CD3d3dy5zbGl3a2kubGlmZYILc2xpd2tpLmxpZmUwCQYDVR0TBAIwADAdBgNV\n"
                + "HSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwQwYDVR0fBDwwOjA4oDagNIYyaHR0\n"
                + "cDovL2NybC5nbG9iYWxzaWduLmNvbS9ncy9nc2RvbWFpbnZhbHNoYTJnMi5jcmww\n"
                + "gZQGCCsGAQUFBwEBBIGHMIGEMEcGCCsGAQUFBzAChjtodHRwOi8vc2VjdXJlLmds\n"
                + "b2JhbHNpZ24uY29tL2NhY2VydC9nc2RvbWFpbnZhbHNoYTJnMnIxLmNydDA5Bggr\n"
                + "BgEFBQcwAYYtaHR0cDovL29jc3AyLmdsb2JhbHNpZ24uY29tL2dzZG9tYWludmFs\n"
                + "c2hhMmcyMB0GA1UdDgQWBBTa6zr31pW4OGJa3SGXhgckC4vyyzAfBgNVHSMEGDAW\n"
                + "gBTqTnzUgC3lFYGGJoyCbcCYpM+XDzANBgkqhkiG9w0BAQsFAAOCAQEAgxFtdGhE\n"
                + "DJqYdFmvrOqMsEtulTAH1dYO7PBNSjH1/aW9jVIEj0AtJvlbEC8zqjQ7OsD6A1Zl\n"
                + "w444e0fhkZ+iCRrOKnptEtdmEUEQpmWsT9ZdS1Uq0OwVF1vsNm+jOSUq+kdS0Nef\n"
                + "n2CzGdUdro2ksGRBJoJhwKug9nRLQdgIk+3HHZv1HulOBqzOz0c/D8CVv2hC734K\n"
                + "y+YVY0P2RJ8PNKyXd1TfckZtLvdKvSLnjjCA+G5Dx4X7YILirztL0BM/g4ik0K2e\n"
                + "qqb8ebps16rmx0wwdesjzK/oI3B97sYVCNI2HhLSO8oRs3/tgg1EU1RXNFviD4DP\n"
                + "zdqKjapPpOKo5A==\n"
                + "-----END CERTIFICATE-----\n";
        return new Buffer()
                .writeUtf8(serverCertificate)
                .inputStream();
    }


    /**
     * Асинхронный запрос на регистрацию Смс-шлюза.
     *
     * @param secret  секретный ключ в заголовке http запроса, по умолчанию должен принимать значение:
     *                {@link LifeSliwkiApi#DEFAULT_SECRET_KEY}
     * @param phone   телефонный номер, который будет зарегистриован как смс-шлюз
     * @param handler {@link Handler} который обрабатывает http код результата асинхронного запроса
     */
    public void sendRegRequest(String secret, String phone, final Handler handler) {
        PhoneRequest phoneRequest = new PhoneRequest();
        phoneRequest.setPhone(phone);
        Call<RegResponse> call = api.registerGate(secret, phoneRequest);
        call.enqueue(new Callback<RegResponse>() {
            @Override
            public void onResponse(Call<RegResponse> call, Response<RegResponse> response) {
                if (response.isSuccessful()) {
                    RegResponse regResponse = response.body();
                    if (regResponse.getCode() != null) {
                        settings.setRegister(true);
                    } else if (regResponse.getDescription() != null) {
                        settings.setRegister(true);
                    }
                    Log.d("Request Reg", regResponse.toString());
                } else {
                    Log.d("Error", "No Success, code " + response.code());
                }
                handler.handleMessage(handler.obtainMessage(response.code()));
            }

            @Override
            public void onFailure(Call<RegResponse> call, Throwable t) {
                Log.d("Error", "" + t.getMessage());
                handler.handleMessage(handler.obtainMessage(LifeSliwkiApi.ERROR_RESPONSE));
            }
        });
    }

    /**
     * Асинхронный запрос на активацию Смс-шлюза.
     *
     * @param phone   телефонный номер, откуда пришел смс-код активации шлюза
     * @param code    смс-код активации шлюза
     * @param handler {@link Handler} который обрабатывает http код результата асинхронного запроса
     */
    public void activateRequest(String phone, String code, final Handler handler) {
        ActivateRequest activateRequest = new ActivateRequest();
        activateRequest.setPhone(phone);
        activateRequest.setCode(code);
        Call<ActivateResponse> call = api.activateGate(LifeSliwkiApi.DEFAULT_SECRET_KEY, activateRequest);
        call.enqueue(new Callback<ActivateResponse>() {
            @Override
            public void onResponse(Call<ActivateResponse> call, Response<ActivateResponse> response) {
                if (response.isSuccessful()) {

                    ActivateResponse activateResponse = response.body();
                    if (activateResponse.isApproved()) {
                        settings.setActivate(true);
                        settings.setSmsReceiver(true);
                        settings.setSecret((activateResponse.getSecretCode()));
                    } else if (!activateResponse.isApproved()) {
                        settings.setActivate(false);
                    }
                    Log.d("Request Activate", activateResponse.toString());
                } else {
                    Log.d("Error", "No Success, code " + response.code());
                }
                if (!settings.getSecret().equals(LifeSliwkiApi.DEFAULT_SECRET_KEY)) {
                    settings.setActivate(true);
                }
                handler.handleMessage(handler.obtainMessage(response.code()));
            }

            @Override
            public void onFailure(Call<ActivateResponse> call, Throwable t) {
                Log.d("Error", "" + t.getMessage());
                handler.handleMessage(handler.obtainMessage(LifeSliwkiApi.ERROR_RESPONSE));
            }
        });
    }

    /**
     * Асинхронный запрос на удаление Смс-шлюза.
     *
     * @param secret  секретный ключ в заголовке http запроса, по умолчанию должен принимать значение:
     *                {@link LifeSliwkiApi#DEFAULT_SECRET_KEY}
     * @param phone   телефонный номер на который был зарегистрирован смс-шлюз
     * @param handler {@link Handler} который обрабатывает http код результата асинхронного запроса
     */
    public void deleteRequest(String secret, String phone, final Handler handler) {
        PhoneRequest phoneRequest = new PhoneRequest();
        phoneRequest.setPhone(phone);
        Call<DeleteResponse> call = api.deleteGate(secret, phoneRequest);
        call.enqueue(new Callback<DeleteResponse>() {
            @Override
            public void onResponse(Call<DeleteResponse> call, Response<DeleteResponse> response) {
                if (response.isSuccessful()) {

                    DeleteResponse deleteResponse = response.body();
                    if (deleteResponse.isStatus()) {
                        settings.setActivate(false);
                        settings.setRegister(false);
                        settings.setSecret(LifeSliwkiApi.DEFAULT_SECRET_KEY);
                    }
                    Log.d("Request delete", deleteResponse.toString());
                } else {
                    Log.d("Error", "No Success, code " + response.code());
                }
                handler.handleMessage(handler.obtainMessage(response.code()));
            }

            @Override
            public void onFailure(Call<DeleteResponse> call, Throwable t) {
                Log.d("Error", "" + t.getMessage());
                handler.handleMessage(handler.obtainMessage(LifeSliwkiApi.ERROR_RESPONSE));
            }
        });
    }

    /**
     * Асинхронный запрос для отправки смс на сервер.
     * Для отправки этого запроса смс-шлюз должен быть активирован.
     *
     * @param secret  секретный ключ в заголовке http запроса, должен быть получен с помощью запроса
     *                {@link #activateRequest(String, String, Handler)}
     * @param phone   телефонный номер, откуда пришло смс
     * @param text    текст смс
     * @param handler {@link Handler} который обрабатывает http код результата асинхронного запроса
     */
    public void receiverRequest(final String secret, String phone, String text, final Handler handler) {
        SmsRequest smsRequest = new SmsRequest();
        smsRequest.setPhone(phone);
        smsRequest.setText(text);
        Call<SmsResponse> call = api.receiver(secret, smsRequest);
        call.enqueue(new Callback<SmsResponse>() {
            @Override
            public void onResponse(Call<SmsResponse> call, Response<SmsResponse> response) {
                if (response.isSuccessful()) {

                    SmsResponse receiverResponse = response.body();
                    settings.setSmsReceiver(receiverResponse.isStatus());
                    Log.d("Request receiver", receiverResponse.toString());
                } else {
                    settings.setSmsReceiver(false);
                    Log.d("Error", "No Success, code " + response.code());
                }
                handler.handleMessage(handler.obtainMessage(response.code()));
            }

            @Override
            public void onFailure(Call<SmsResponse> call, Throwable t) {
                Log.d("Error", "" + t.getMessage());
                settings.setSmsReceiver(false);
                handler.handleMessage(handler.obtainMessage(LifeSliwkiApi.ERROR_RESPONSE));
            }
        });
    }

    /**
     * Асинхронный запрос для проверки соединения с сервером.
     * Для отправки этого запроса смс-шлюз должен быть активирован.
     *
     * @param secret  секретный ключ в заголовке http запроса, должен быть получен с помощью запроса
     *                {@link #activateRequest(String, String, Handler)}
     * @param handler {@link Handler} который обрабатывает http код результата асинхронного запроса
     */
    public void pingHost(String secret, final Handler handler) {

        Call<PingResponse> call = api.pingHost(secret);
        call.enqueue(new Callback<PingResponse>() {
            @Override
            public void onResponse(Call<PingResponse> call, Response<PingResponse> response) {
                if (response.isSuccessful()) {
                    PingResponse pingResponse = response.body();
                    settings.setAvailable(pingResponse.isStatus());

                    settings.setLastTimeServerAvailable(getCurrentTimeString());
                    Log.d("Request ping", pingResponse.toString());
                } else {
                    Log.d("Request ping", "No Success, code " + response.code());
                    settings.setAvailable(false);
                }
                handler.handleMessage(handler.obtainMessage(response.code()));
            }

            @Override
            public void onFailure(Call<PingResponse> call, Throwable t) {
                Log.d("Error", "" + t.getMessage());
                settings.setAvailable(false);
                handler.handleMessage(handler.obtainMessage(LifeSliwkiApi.ERROR_RESPONSE));
            }
        });
    }
    public static String getCurrentTimeString(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
        return date.format(c.getTime());
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public GlobalSetting getSettings() {
        return settings;
    }

    public void setSettings(GlobalSetting settings) {
        this.settings = settings;
    }
}

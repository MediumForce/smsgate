package life.sliwki.smsgate.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Класс ответа на запрос {@link life.sliwki.smsgate.interfaces.LifeSliwkiApi#pingHost(String)}
 * Created by Medium on 22.07.2016.
 */
public class PingResponse {

    /**
     * Доступность сервера
     */
    @SerializedName("status")
    @Expose
    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PingResponse{" +
                "status=" + status +
                '}';
    }
}

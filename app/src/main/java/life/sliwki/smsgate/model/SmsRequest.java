package life.sliwki.smsgate.model;

import com.google.gson.annotations.SerializedName;

/**
 * Класс тела запроса {@link life.sliwki.smsgate.interfaces.LifeSliwkiApi#receiver(String, SmsRequest)}
 * Created by Medium on 21.07.2016.
 */
public class SmsRequest {

    /**
     * Телефонный номер откуда пришло смс
     */
    @SerializedName("phone_num")
    private String phone;

    /**
     * Текст смс
     */
    @SerializedName("text")
    private String text;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

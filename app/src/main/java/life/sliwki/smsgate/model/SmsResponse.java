package life.sliwki.smsgate.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Класс ответа на запрос {@link life.sliwki.smsgate.interfaces.LifeSliwkiApi#receiver(String, SmsRequest)}
 * Created by Medium on 22.07.2016.
 */
public class SmsResponse {
    /**
     * Статус принятяи смс на сервере
     */
    @SerializedName("status")
    @Expose
    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SmsResponse{" +
                "status=" + status +
                '}';
    }
}

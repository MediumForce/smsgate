package life.sliwki.smsgate.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Класс ответа на запрос {@link life.sliwki.smsgate.interfaces.LifeSliwkiApi#activateGate(String, ActivateRequest)}
 * Created by Medium on 22.07.2016.
 */
public class ActivateResponse {

    /**
     * Уникальный секретный код использующийся для отправки смс на сервер и проверки соединения с сервером
     */
    @SerializedName("secret_code")
    @Expose
    private String secretCode;

    /**
     * Статус подтверждения регистрации, при успешном запросе будет иметь значение true
     */
    @SerializedName("approved")
    @Expose
    private boolean approved;

    /**
     * Присутствует в ответе только это поле, если запрос был не успешен.
     */
    @SerializedName("status")
    @Expose
    private boolean status;

    public String getSecretCode() {
        return secretCode;
    }

    public void setSecretCode(String secretCode) {
        this.secretCode = secretCode;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ActivateResponse{" +
                "secretCode='" + secretCode + '\'' +
                ", approved=" + approved +
                ", status=" + status +
                '}';
    }
}

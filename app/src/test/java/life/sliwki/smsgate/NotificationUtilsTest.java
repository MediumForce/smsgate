package life.sliwki.smsgate;

import android.app.NotificationManager;
import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by Medium on 31.07.2016.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class NotificationUtilsTest {

    private NotificationManager notificationManager;

    @Before
    public void setUp() {
        notificationManager = (NotificationManager) RuntimeEnvironment.application.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Test
    public void testNotifyInternetError() throws Exception {
        NotificationUtils utils = new NotificationUtils(RuntimeEnvironment.application);
        utils.sendNotifyInternetError();
        assertEquals(1, shadowOf(notificationManager).size());
        assertNotNull("notification should be send ", shadowOf(notificationManager).getNotification(null, NotificationUtils.NOTIFY_INTERNET_ERROR));
    }

    @Test
    public void testNotifyServerError() throws Exception {
        NotificationUtils utils = new NotificationUtils(RuntimeEnvironment.application);
        utils.sendNotifyServerError();
        assertEquals(1, shadowOf(notificationManager).size());
        assertNotNull("notification should be send ", shadowOf(notificationManager).getNotification(null, NotificationUtils.NOTIFY_SERVER_ERROR));
    }

    @Test
    public void testNotifyReceiveSmsPermission() throws Exception {
        NotificationUtils utils = new NotificationUtils(RuntimeEnvironment.application);
        utils.sendNotifyReceiveSmsPermission();
        assertEquals(1, shadowOf(notificationManager).size());
        assertNotNull("notification should be send ", shadowOf(notificationManager).getNotification(null, NotificationUtils.NOTIFY_RECEIVE_SMS_PERMISSIONS));
    }

    @Test
    public void testNotifyForbiddenError() throws Exception {
        NotificationUtils utils = new NotificationUtils(RuntimeEnvironment.application);
        utils.sendNotifyForbiddenError();
        assertEquals(1, shadowOf(notificationManager).size());
        assertNotNull("notification should be send ", shadowOf(notificationManager).getNotification(null, NotificationUtils.NOTIFY_FORBIDDEN_ERROR));
    }
}

package life.sliwki.smsgate.receivers;

import android.content.Intent;
import android.provider.Telephony;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import life.sliwki.smsgate.BuildConfig;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Medium on 31.07.2016.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class SMSMonitorTest {

    @Test
    public void registerServiceOnDeviceBootComplete() throws Exception {
        Intent intent = new Intent(Telephony.Sms.Intents.SMS_RECEIVED_ACTION);

        ShadowApplication application = ShadowApplication.getInstance();

        assertTrue("SMS Listener not registered ",
                application.hasReceiverForIntent(intent));
    }

    @Test
    public void shouldCheckReceiverCodeMessage() throws Exception {
        assertTrue("Check one number", SMSMonitor.isReceiverCode("1"));
        assertTrue("Check few numbers", SMSMonitor.isReceiverCode("1123521"));
        assertFalse("Not check non-number symbol", SMSMonitor.isReceiverCode("112-521"));
        assertFalse("Not check reg code", SMSMonitor.isReceiverCode("reg-124"));
        assertFalse("Not check alphabet string code", SMSMonitor.isReceiverCode("abas"));
        assertFalse("Not check empty code", SMSMonitor.isReceiverCode(""));
    }

    @Test
    public void shouldCheckActivateCodeMessage() throws Exception {
        assertTrue("Check reg code with one number", SMSMonitor.isActivateCode("reg-1"));
        assertTrue("Check reg code with few numbers", SMSMonitor.isActivateCode("reg-800111"));
        assertFalse("Not check reg code without numbers", SMSMonitor.isActivateCode("reg-"));
        assertFalse("Not check reg code with non-number symbol", SMSMonitor.isActivateCode("reg-1@2a-"));
        assertFalse("Not check reg code without reg prefix", SMSMonitor.isActivateCode("800111243"));
        assertFalse("Not check empty reg code", SMSMonitor.isActivateCode(""));
    }
}

package life.sliwki.smsgate.model;

import com.google.gson.annotations.SerializedName;

/**
 * Класс тела запроса {@link life.sliwki.smsgate.interfaces.LifeSliwkiApi#activateGate(String, ActivateRequest)}
 * Created by Medium on 21.07.2016.
 */
public class ActivateRequest {

    /**
     * Телефонный номер откуда пришло смс на активацию
     */
    @SerializedName("phone_num")
    private String phone;

    /**
     * Код активации смс-шлюза
     */
    @SerializedName("verification_code")
    private String code;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

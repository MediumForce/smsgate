package life.sliwki.smsgate.services;

import android.Manifest;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import life.sliwki.smsgate.GlobalSetting;
import life.sliwki.smsgate.HttpRequest;
import life.sliwki.smsgate.NotificationUtils;
import life.sliwki.smsgate.interfaces.LifeSliwkiApi;

/**
 * Класс {@link Service} отправляющий ping запросы на сервер
 */
public class PingService extends Service implements Runnable {

    /**
     * Параметр для {@link Intent} сообщающий что UI в {@link life.sliwki.smsgate.MainActivity} что
     * UI надо обновиться в соответствием с результатами запросов
     */
    public static final String PING_RESULT = "life.sliwki.smsgate.services.PingService.REQUEST_PROCESSED";

    /**
     * Текстовый тэг для записи логов
     */
    private static final String TAG = "SmsGateService";

    /**
     * Время задержки между двумя ping запросами
     */
    private static final long FIVE_MIN = 5 * 60 * 1000L;

    /**
     * Переменная настроек для получения секретного кода и его проверки
     */
    private GlobalSetting settings;

    /**
     * Переменная позволяющая делать запросы к серверу
     */
    private HttpRequest http;

    /**
     * Обработчик ответов от сервера
     */
    private PingHandler handler;

    /**
     * {@link NotificationManager} для работы с уведомлениями
     */
    private NotificationManager mNotificationManager;
    /**
     * {@link NotificationUtils} для вызова уведомлений
     */
    private NotificationUtils notificationUtils;

    /**
     * Номер ping запроса
     */
    private long numberOfPingRequest = 0;

    /**
     * Поток в котором будут запущены ping запросы
     */
    private volatile Thread runner;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Context context = getApplicationContext();
        handler = new PingHandler(context);
        settings = new GlobalSetting(context);
        http = new HttpRequest(context);
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationUtils = new NotificationUtils(context);
        startForeground(NotificationUtils.NOTIFY_FOREGROUND,notificationUtils.buildNotifyForeground());
    }

    @Override
    public void onDestroy() {
        stopThread();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startThread();
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    /**
     * Если смс-шлюз был активирован то будут отослаться Ping запросы раз в 5 минут
     */
    @Override
    public void run() {
        while (Thread.currentThread() == runner) {
            checkPermissions();
            if (settings.isActivate()) {
                Log.v(TAG, "Ping #" + numberOfPingRequest);
                http.pingHost(settings.getSecret(), handler);
                numberOfPingRequest++;
            }
            try {
                Thread.sleep(FIVE_MIN);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * запускает поток с ping запросами
     */
    public synchronized void startThread() {
        if (runner == null) {
            runner = new Thread(this);
            runner.start();
        }
    }

    /**
     * останавливает поток с ping запросами
     */
    public synchronized void stopThread() {
        if (runner != null) {
            Thread moribund = runner;
            runner = null;
            moribund.interrupt();
        }
    }

    /**
     * проверяет полученны ли все разрешения для приложения, если нет то отображает соответствующие уведомления
     */
    private void checkPermissions(){
        if(!isPermissionGranted(Manifest.permission.RECEIVE_SMS)){
            notificationUtils.sendNotifyReceiveSmsPermission();
        }
        else
            mNotificationManager.cancel(NotificationUtils.NOTIFY_RECEIVE_SMS_PERMISSIONS);
    }

    /**
     * Проверяет полученое ли конкретное разрешение у системы
     * @param permission название разрешения
     * @return true если такое разрешение приложением получено, иначе false
     */
    private boolean isPermissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(this, permission)
                == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Класс обрабатывающий результаты ping запросов в {@link PingService}
     */
    private static class PingHandler extends Handler {

        private LocalBroadcastManager broadcaster;
        private NotificationUtils notificationUtils;
        private NotificationManager mNotificationManager;

        /**
         * Создает экземпляр {@link Handler} для обработки результатов ping запросов.
         * @param context в котором создается {@link Handler}
         */
        public PingHandler(Context context) {
            this.notificationUtils = new NotificationUtils(context);
            this.broadcaster = LocalBroadcastManager.getInstance(context);
            this.mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        /**
         * Функция выдающая уведомление соответствующие коду состояния http запроса в случае если {@link Message#what}
         * не является успешным кода запроса.<br>Если запрос был отправлен успешно то отправляет на
         * UI {@link life.sliwki.smsgate.MainActivity} {@link Intent} об этом и отменяет уведомления
         * не соответвующие текущему состоянию приложения
         * @param msg {@link Message} содержащий код состояния http запроса
         */
        @Override
        public void handleMessage(Message msg) {
            if (msg.what < 200 || msg.what > 300) {
                if (!notificationUtils.isInternetConnection()) {
                    notificationUtils.sendNotifyInternetError();
                } else if (msg.what >= 500) {
                    notificationUtils.sendNotifyServerError();
                } else if (msg.what == LifeSliwkiApi.ERROR_FORBIDDEN) {
                    notificationUtils.sendNotifyForbiddenError();
                }
            }
            else {
                mNotificationManager.cancel(NotificationUtils.NOTIFY_INTERNET_ERROR);
                mNotificationManager.cancel(NotificationUtils.NOTIFY_SERVER_ERROR);
                mNotificationManager.cancel(NotificationUtils.NOTIFY_FORBIDDEN_ERROR);
            }

            Intent intent = new Intent(PING_RESULT);
            intent.putExtra("what", msg.what);
            broadcaster.sendBroadcast(intent);
        }

    }

}

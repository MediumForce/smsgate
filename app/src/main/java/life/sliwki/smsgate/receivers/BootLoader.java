package life.sliwki.smsgate.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import life.sliwki.smsgate.services.PingService;

/**
 * Класс {@link BroadcastReceiver} загружающий {@link PingService}.
 * Created by Medium on 26.07.2016.
 */
public class BootLoader extends BroadcastReceiver {
    /**
     * Загружает {@link PingService} при включении телефона, установки приложения и его обновления.
     *
     * @param context {@link Context} с которым receiver запускается
     * @param intent  {@link Intent} который был перехвачен receiverом
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null && (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)
                || intent.getAction().equalsIgnoreCase(Intent.ACTION_PACKAGE_ADDED)
                || intent.getAction().equalsIgnoreCase(Intent.ACTION_PACKAGE_REPLACED))) {
            Intent serviceIntent = new Intent(context, PingService.class);
            context.startService(serviceIntent);
        }
    }
}
